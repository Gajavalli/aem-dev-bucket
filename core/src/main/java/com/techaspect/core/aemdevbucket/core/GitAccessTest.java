package com.techaspect.core.aemdevbucket.core;

import java.io.File;
import java.io.IOException;

import org.eclipse.jgit.api.CreateBranchCommand;
import org.eclipse.jgit.api.Git;

/**
 * Class for retrieving file history from GIT Inspired by
 * http://wiki.eclipse.org/JGit/User_Guide
 * http://stackoverflow.com/questions/1685228/how-to-cat-a-file-in-jgit
 * http://www.programcreek.com/java-api-examples/index.php?api=org.eclipse.jgit.treewalk.TreeWalk
 */
public class GitAccessTest {
	public static void main(String args[]){
		System.out.println("in main");
		File file = new File("E:AEM/POC/Self-POC/repo/aem-dev-bucket");
		System.out.println("file object :"+file);
		try {
			Git git = Git.open(file);
			System.out.println("git :"+git);
			CreateBranchCommand cb = git.branchCreate();
			cb.setName("test");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
